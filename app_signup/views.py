from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render

def logout_view(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/')

def login_view(request):
    return render(request, 'login.html')