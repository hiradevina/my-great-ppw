from django.shortcuts import render
from .forms import StatusForm, SubscribeForm
from django.http import HttpResponseRedirect
from .models import Status, Subscriber
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict
import json

response = {}
# Create your views here.

def landing_page(request):
    response = {}
    all_stats = Status.objects.all()
    response['all_stats'] = all_stats
    
    response['forms'] = StatusForm()
    

    return render(request, 'landingpage.html', response)

def add_status(request):
    response = {}
    form = StatusForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        stats = Status(message=response['message'])
        stats.save()
        return HttpResponseRedirect('/landingpage')
    else:
        return HttpResponseRedirect('/landingpage')

def profile(request):
    response = {}
    return render(request, 'profile.html', response)

  
def subscribe(request):
    response = {}
    response["forms"] = SubscribeForm()
    return render(request, 'subscribe.html', response)


@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    submited_form = SubscribeForm(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscriber(name=cd['name'], password=cd['password'], email=cd['email'])
        new_subscriber.save()
        data = {'name': cd['name'], 'password': cd['password'], 'email': cd['email']}
    return JsonResponse(data)

def getallsubs(request):
    data_json = serializers.serialize('json', Subscriber.objects.all())
    return HttpResponse(data_json, content_type='aplication/json')

def subspage(request):
    return render(request, "allsubs.html") 
@csrf_exempt
def delsub(request):
    if 'email' in request.POST:
        print('mantap')
        email = request.POST['email']
        lele = Subscriber.objects.filter(email=email)
        lele.delete()
        return HttpResponseRedirect("/allsubs")
    else:
        print('ngapa DAH')
        return HttpResponseRedirect("/allsubs")

