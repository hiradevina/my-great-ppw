from django.urls import path
from .views import *

app_name = "app_story6"
urlpatterns = [
    path('landingpage', landing_page, name="landingpage"),
    path('add_status',add_status, name="add_status"),
    path('', profile, name="hi-here"),
    path('subscribe', subscribe, name="subscribe"),
    path('validate', validate),
    path('success', success, name="success"),
    path('allsubs', getallsubs, name="allsubs"),
    path('subscribers', subspage, name="subscribers"),
    path('delsub', delsub),
]