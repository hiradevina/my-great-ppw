from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import Status, Subscriber
from .forms import StatusForm, SubscribeForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.utils.encoding import force_text


# Create your tests here.

class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('/landingpage')
        self.assertEqual(response.status_code, 200)
    def test_story6_using_landing_page_func(self):
        found = resolve('/landingpage')
        self.assertEqual(found.func, landing_page)
    def test_landing_page_status_can_create_object(self):
        Status.objects.create(message = "SMNGT")
        count_all_stats = Status.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control', form.as_p())
    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )
    def test_lab6_post_success_and_render_the_result(self):
        test = 'capeq'
        response_post = Client().post('/add_status', {'message': test})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/landingpage')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    def test_lab6_post_error_and_render_the_result(self):
        test = 'capeq'
        response_post = Client().post('/add_status', {'message': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/landingpage')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    def test_landing_page_has_greeting(self):
        response = Client().get('/landingpage')
        landing_page_content = 'Hello, Apa kabar?'
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)
    def test_profile_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_profile_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)
    def test_profile_page_has_name(self):
        response = Client().get('/')
        profile_page_content = 'Fakhira Devina'
        html_response = response.content.decode('utf8')
        self.assertIn(profile_page_content, html_response)

    def test_profile_has_education(self):
        response = Client().get('/')
        profile_page_content = 'Fasilkom UI'
        html_response = response.content.decode('utf8')
        self.assertIn(profile_page_content, html_response)
    
    
    
    def test_subscribe_has_url(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)
    def test_subscribe_using_subscribe_func(self):
        found = resolve('/subscribe')
        self.assertEqual(found.func, subscribe)
    def test_subscribe_can_create_object(self):
        Subscriber.objects.create(name="ginting", password="momota", email="lumpiatelor@gmail.com")
        count_all_stats = Subscriber.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    def test_fsubscribe_form_has_placeholder_and_css_classes(self):
        form = SubscribeForm()
        self.assertIn('class="form-control', form.as_p())
    def test_subscribe_form_validation_for_blank_items(self):
        form = SubscribeForm(data={'name': '', 'password': '', 'email':'' })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["Please enter your name"]
        )
        self.assertEqual(
            form.errors['email'],
            ["Please enter a valid email"]
        )
        self.assertEqual(
            form.errors['password'],
            ["This field is required."]
        )
    def test_subscribe_password_less_than_6_char(self):
        form = SubscribeForm(data={'name': 'ginting', 'password': 'momo', 'email':'lumpiatelor@gmail.com' })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],
            ["password must be longer than 5 characters"]
        )
    def test_double_subscriber(self):
        nama = 'ginting'
        password = 'momotey'
        email = 'lumpiatelor@gmail.com'
        go = Client().post('/validate', {'email': email})
        # must respond false
        self.assertJSONEqual(
            force_text(go.content),
            {'not_valid': False}
        )
        # button not disabled, send data to views
        Client().post('/success', {'name': nama, 'password': password, 'email': email})
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)
        # email used second time
        go = Client().post('/validate', {'email': email})
        # must respond true, email already taken
        self.assertJSONEqual(
            str(go.content, encoding='utf8'),
            {'not_valid': True}
        )
        # not making any new data
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)
class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_status_and_post_status(self):
         selenium = self.selenium
         # Opening the link we want to test
         selenium.get('https://here-a.herokuapp.com/landingpage')
         # find the form element
         status = selenium.find_element_by_id('id_message')

         submit = selenium.find_element_by_tag_name('button')

         # Fill the form with data
         status.send_keys('Coba Coba')

         # submitting the form
         submit.send_keys(Keys.RETURN)
         # check status on page
         self.assertIn('Coba Coba', selenium.page_source)

    def test_web_background_is_white(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://here-a.herokuapp.com/landingpage')
        body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertEqual(body, "rgba(237, 239, 240, 1)")
    
    def test_web_font_is_Helvetica_Neue(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://here-a.herokuapp.com/landingpage')
        font = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        self.assertIn("Helvetica Neue", font)

    def test_input_status_box_location(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://here-a.herokuapp.com/landingpage')
        statusbox = selenium.find_element_by_id('id_message')
        # self.assertEqual(statusbox.location['x'], 222)
        # self.assertEqual(statusbox.location['y'], 88)

    def test_input_submit_button_location(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://here-a.herokuapp.com/landingpage')
        submit = selenium.find_element_by_tag_name('button')
        # self.assertEqual(submit.location['x'], 349)
        # self.assertEqual(submit.location['y'], 142)