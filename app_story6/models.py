from django.db import models

# Create your models here.
class Status(models.Model):
    message = models.CharField(max_length = 300)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)


class Subscriber(models.Model):
    name = models.CharField(max_length = 50)
    password = models.CharField(max_length = 20)
    email = models.CharField(max_length = 50)