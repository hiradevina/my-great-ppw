$(document).ready(function() {
    
    $("#changetheme").click(function(){
      if($("body").css("background-color") == "rgb(245, 245, 245)") {
        $("body").css("background-color", "#10171e");
        $(".prof").css("color", "#ffffff");  
        $(".accordion").css("background-color", "#243447");
        $(".accordion").css("color", "#ffffff");
        $(".panel").css("background-color", "#243447");
        $("#changetheme").removeClass("btn-dark");
        $("#changetheme").addClass("btn-light");
      } else {
        $("body").css("background-color", "#f5f5f5");
        $(".prof").css("color", "#212529");
        $(".accordion").css("background-color", "#eee");
        $(".accordion").css("color", "#444");
        $(".panel").css("background-color", "white");
        $("#changetheme").removeClass("btn-light");
        $("#changetheme").addClass("btn-dark");
      }     
    });
    
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
  }
  
});
$("img").tooltip();

// function change() {
    
// }

// function reverse() {
//     $("body").css("background-color", "#f5f5f5");
//     $(".prof").css("color", "#212529");
// }

