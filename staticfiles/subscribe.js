$(document).ready(function() {
    var name = "";
    var password = "";
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_password").change(function() {
        password = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email is already taken");
                } else {
                    if(name.length > 0 && password.length > 5) {
                        $("#submit_subscribe").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit_subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hello " + data.name,
                    text: 'Thanks for subscribing. Redirecting you back to home...',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500,
                    footer: '<a href=\"/subscribers\">See all subscribers</a>'
                    }).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! Redirecting you back to home...',
                    showConfirmButton: false,
                    timer: 2500
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});