$(document).ready(function() {
    $("#id_email").change(function() {
        var name = $("#id_name").val();
        var password = $("#id_password").val();
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                if(data.not_valid) {
                    alert("This email is already taken");
                } else {
                    if(name.length > 0 && password.length > 5) {
                        console.log('iyejingg')
                        $("#submit_subscribe").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit_subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'html',
            success: function(){
                swal(
                    'Success!',
                    'Your data is recorded. Redirecting you back to home...',
                    'success'
                    ).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});