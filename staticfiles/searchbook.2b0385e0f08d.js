$(document).ready(function() {

    $(".fav").each(function() {
        $(this).click(function() {
            if($(this).hasClass("far")) {
                $(this).removeClass("far");
                $(this).removeClass("fa-star");
                $(this).addClass("fas");
                $(this).addClass("fa-star");
                var cls= $(this).closest('tr').attr('id');
                
                console.log(cls);
                var judul = $('#'+cls).find("td.judul").text();
                var link = $('#'+cls).find("a").attr("href");
                var img = $('#'+cls).find("img").attr("src");
                var author =  $('#'+cls).find("td.author").text();
                eonsole.log(img);

                $.ajax({
                        url: "/books/add-fav",
                        data: {
                            'judul': judul,
                            'link' : link,
                            'img' : img,
                            'author': author
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#count").empty().text(data.length);
                        }
                    });
            }else {
                $(this).removeClass("fas");
                $(this).removeClass("fa-star");
                $(this).addClass("far");
                $(this).addClass("fa-star");
                var cls= $(this).closest('tr').attr('id');
                
                console.log(cls);
                var judul = $('#'+cls).find("td.judul").text();
                var link = $('#'+cls).find("a").attr("href");
                var img = $('#'+cls).find("img").attr("src");
                var author =  $('#'+cls).find("td.author").text();
                console.log(img);

                $.ajax({
                        url: "/books/del-fav",
                        data: {
                            'judul': judul,
                            'link' : link,
                            'img' : img,
                            'author': author
                        },
                        dataType: 'json',
                        success: function(dell) {
                            $("#count").empty().text(dell.length);
                        }
                    });
            }
        });
    });
});