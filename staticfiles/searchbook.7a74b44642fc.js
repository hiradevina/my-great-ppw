$(document).ready(function() {

    $(".fav").each(function() {
        $(this).click(function() {
            if($(this).hasClass("far")) {
                $(this).removeClass("far");
                $(this).removeClass("fa-star");
                $(this).addClass("fas");
                $(this).addClass("fa-star");
                var cls= $(this).closest('tr').attr('id');
                
                console.log(cls);
                var judul = $('#'+cls).find("td.judul").text();
                var link = $('#'+cls).find("a").attr("href");
                var img = $('#'+cls).find("img").attr("src");
                var author =  $('#'+cls).find("td.author").text();
                console.log(img);

                $.ajax({
                        url: "/books/add-fav",
                        data: {
                            'judul': judul,
                            'link' : link,
                            'img' : img,
                            'author': author
                        },
                        dataType: 'json',
                        success: function(data) {
                            console.log('masukcoy')
                            console.log(data)
                            $("#count").empty().text(data);
                        }
                    });
            }else {
                $(this).removeClass("fas");
                $(this).removeClass("fa-star");
                $(this).addClass("far");
                $(this).addClass("fa-star");
                var cls= $(this).closest('tr').attr('id');
                
                console.log(cls);
                var judulx = $('#'+cls).find("td.judul").text();
                var linkx = $('#'+cls).find("a").attr("href");
                var imgx = $('#'+cls).find("img").attr("src");
                var authorx =  $('#'+cls).find("td.author").text();
                console.log(imgx);

                $.ajax({
                        url: "/books/del-fav",
                        data: {
                            judul: judulx,
                            link : linkx,
                            img : imgx,
                            author: authorx
                        },
                        method: 'POST',
                        dataType: 'json',
                        success: function(dell) {
                            $("#count").empty().text(dell);
                        }
                    });
            }
        });
    });
});