$(document).ready(function() {
    $("#id_title").change(function(){
        $("#result").empty();
        var title = $(this).val();
        console.log(title);
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + title,
            dataType: "json",
            
            success: function(result){
                console.log(result);
                
                $("#result").append("<thead><tr><th scope=\"col\">No</th><th scope=\"col\">Favorite</th><th scope=\"col\">Book</th><th scope=\"col\">Title</th><th scope=\"col\">Author</th></tr></thead><tbody>");
                for (i=0; i<result.items.length; i++) {

                    var judult = "<td class=\"judul\">" + result.items[i].volumeInfo.title + "</td>";
                    var authort = "<td class=\"author\">"+ result.items[i].volumeInfo.authors + "</td>";
                    var nomor = "<tr id=\"item-"+ i+1 + "\">";
                    var roww = "<th scope="+ "\"row\">" + (i+1) + "</th>";
                    var star = "<td><i class=\"item" + (i+1) + " fav far fa-star\"></td>"
                    var link = "<td><a target=\"_blank\" href=\""+ result.items[i].volumeInfo.infoLink + "\">"+ "<img src=\"" + result.items[i].volumeInfo.imageLinks.thumbnail + "\">" + "</a></td>"
                    $("#result").append(nomor + roww + star + link + judult + authort + "</tr>");
                }
                $("#result").append("</tbody>");
                $("i").each(function() {
                    $(this).click(function() {
                        if($(this).hasClass("far")) {
                            $(this).removeClass("far");
                            $(this).removeClass("fa-star");
                            $(this).addClass("fas");
                            $(this).addClass("fa-star");
                            var cls= $(this).closest('tr').attr('id');
                            
                            console.log(cls);
                            var judul = $('#'+cls).find("td.judul").text();
                            var link = $('#'+cls).find("a").attr("href");
                            var img = $('#'+cls).find("img").attr("src");
                            var author =  $('#'+cls).find("td.author").text();
                            console.log(img);

                            $.ajax({
                                 url: "/books/add-fav",
                                 data: {
                                     'judul': judul,
                                     'link' : link,
                                     'img' : img,
                                     'author': author
                                 },
                                 dataType: 'json',
                                 success: function(data) {
                                     $("#count").empty().text(data.length);
                                 }
                             });
                        }else {
                            $(this).removeClass("fas");
                            $(this).removeClass("fa-star");
                            $(this).addClass("far");
                            $(this).addClass("fa-star");
                            var cls= $(this).closest('tr').attr('id');
                            
                            console.log(cls);
                            var judul = $('#'+cls).find("td.judul").text();
                            var link = $('#'+cls).find("a").attr("href");
                            var img = $('#'+cls).find("img").attr("src");
                            var author =  $('#'+cls).find("td.author").text();
                            console.log(img);

                            $.ajax({
                                 url: "/books/del-fav",
                                 data: {
                                     'judul': judul,
                                     'link' : link,
                                     'img' : img,
                                     'author': author
                                 },
                                 dataType: 'json',
                                 success: function(data) {
                                     $("#count").empty().text(data.length);
                                 }
                             });
                        }
                    } );
                });
            }
        });
    });

});