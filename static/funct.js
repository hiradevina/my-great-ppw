$(document).ready(function() {
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
  $(".section").css("min-height", $(window).height() );
  //$(".section").css("min-width", $(window).width() );
  $("#me").css("min-height", $(window).height());
  // navbar active
  var sections = $('section')
  , nav = $('nav')
  , nav_height = nav.outerHeight();
 
$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
 
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
 
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a').removeClass('current');
      sections.removeClass('current');
 
      $(this).addClass('current');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('current');
    }
  });
});

nav.find('a').on('click', function () {
  var $el = $(this)
    , id = $el.attr('href');
 
  $('html, body').animate({
    scrollTop: $(id).offset().top
  }, 500);
 
  return false;
});
  $('.progress-bar').each(function() {

    var valueNow = $(this).attr('aria-valuenow');
  
    $(this).animate({
      
      width: valueNow + '%',
  
      percent: 100
  
    }, {
  
      progress: function(a, p, n) {
  
        $(this)
          .css('width', (valueNow * p + '%'))
          .html(Math.round(valueNow * p) + '%');
  
      }
  
    });
  
  });
  if(isMobile){
    $("#mobile").css('display', 'block');
    $("body").css("background", 'url("https://i.imgur.com/x5YXUfx.jpg") no-repeat center center fixed');
    $("body").css("-webkit-background-size", 'cover');
    $("body").css("-moz-background-size", 'cover');
    $("body").css("-o-background-size", 'cover');
    $("body").css("background-size", 'cover');
    
    $("body").css("height", '100%');
    $("body").css("overflow", 'cover');
    
    $("#desktop").css('display', 'none');
    $("#me").css('min-height', '0px')
  } else{
    $("#mobile").css('display', 'none');
    $("#desktop").css('display', 'flex');
    // $("#hello").hover(function(){
    //   if($("#name").css("display") == "none"){
    //     $("#name").show('slow');
    //     $("#npm").show('slow');
    //   }
    //   else {
    //     $("#name").hide('slow');
    //     $("#npm").hide('slow');
    //   }
    // });
    // $("#study").hover(function(){
    //   if($("#fasilkom").css("display") == "none"){
    //     $("#fasilkom").show('slow');
    //   }
    //   else { $("#fasilkom").hide('slow');}
    // });
    // $("#like").hover(function(){
    //   if($("#listen").css("display") == "none"){
    //     $("#listen").show('slow');
    //   }
    //   else { $("#listen").hide('slow');}
    // });
    // $("#find").hover(function(){
      
    //     $("#soc").show('slow');
      
      
    // });
  }
  
  
  
  setTimeout(function() {
    $('#preloader').fadeOut('slow');
  }, 1500);
  $("#changetheme").click(function(){
    if($("body").css("background-color") == "rgb(245, 245, 245)") {
      $("body").css("background-color", "#10171e");
      $("body").css("color", "#f5f5f5");
      $(".prof").css("color", "#ffffff");  
      $(".accordion").css("background-color", "#243447");
      $(".accordion").css("color", "#ffffff");
      $(".panel").css("background-color", "#243447");
      $("#changetheme").removeClass("btn-dark");
      $("#changetheme").addClass("btn-light");
    } else {
      $("body").css("background-color", "#f5f5f5");
      $("body").css("color", "#10171e");
     
      $(".prof").css("color", "#212529");
      $(".accordion").css("background-color", "#eee");
      $(".accordion").css("color", "#444");
      $(".panel").css("background-color", "white");
      $("#changetheme").removeClass("btn-light");
      $("#changetheme").addClass("btn-dark");
    }     
  });
  
  
  
  var acc = document.getElementsByClassName("accordion");
  var i;
  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      } 
    });
  }
  

	 
	  'use strict';
	 
	  // define variables
	  var items = document.querySelectorAll(".timeline li");
	 
	  // check if an element is in viewport
	  // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport

	  function isElementInViewport(el) {
	    var rect = el.getBoundingClientRect();
	    return (
	      rect.top >= 0 &&
	      rect.left >= 0 &&
	      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
	      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
	    );
	  }
	 
	  function callbackFunc() {
	    for (var i = 0; i < items.length; i++) {
	      if (isElementInViewport(items[i])) {
	        items[i].classList.add("in-view");
	      }
	    }
	  }
	 
	  // listen for events
	  window.addEventListener("load", callbackFunc);
	  window.addEventListener("resize", callbackFunc);
	  window.addEventListener("scroll", callbackFunc);
   

    $(".scroll-down").each(function() {
      $(this).click(function() {
        var ini = $(this).closest("section");
        $('html, body').animate({
          scrollTop: $(ini).offset().top
        },200);
      })
      })
      
      $('#myCarousel').carousel({
        interval: 5000
      });
    
      // Control buttons
      $('.next').click(function () {
        $('.carousel').carousel('next');
        return false;
      });
      $('.prev').click(function () {
        $('.carousel').carousel('prev');
        return false;
      });
    
      // On carousel scroll
      $("#myCarousel").on("slide.bs.carousel", function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $(".carousel-item").length;
        if (idx >= totalItems - (itemsPerSlide - 1)) {
          var it = itemsPerSlide -
              (totalItems - idx);
          for (var i = 0; i < it; i++) {
            // append slides to end 
            if (e.direction == "left") {
              $(
                ".carousel-item").eq(i).appendTo(".carousel-inner");
            } else {
              $(".carousel-item").eq(0).appendTo(".carousel-inner");
            }
          }
        }
      });
    
    
	});
  
// function change() {
    
// }

// function reverse() {
//     $("body").css("background-color", "#f5f5f5");
//     $(".prof").css("color", "#212529");
// }

