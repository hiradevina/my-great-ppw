from django.db import models
# Create your models here.
class FavBook(models.Model):
    title = models.CharField(max_length = 100)
    img = models.CharField(max_length=200)
    link = models.CharField(max_length = 200)
    author = models.CharField(max_length=200)