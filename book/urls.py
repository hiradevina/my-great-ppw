from django.urls import path, include
from .views import *


app_name = "book"
urlpatterns = [
    path('', search_book, name="searchbook"),
    path('add-fav', add_fav, name="add-fav"),
    path('del-fav', del_fav, name="del-fav"),
    path('my-fav', my_fav, name="fav"),
]