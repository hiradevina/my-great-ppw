from django.contrib import admin
from .models import Places, Record
# Register your models here.
admin.site.register(Places)
class RecordAdmin(admin.ModelAdmin):
    readonly_fields = ('date_modified',)
admin.site.register(Record, RecordAdmin)