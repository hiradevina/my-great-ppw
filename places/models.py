from django.db import models

# Create your models here.
class Places(models.Model):
    place = models.TextField(max_length=30, primary_key=True)
    img = models.CharField(max_length=200)
    desc = models.TextField(max_length=5000)
    date_modified = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

class Record(models.Model):
    nama = models.CharField(max_length=50)
    place = models.TextField(max_length=50)
    date_modified =  models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)