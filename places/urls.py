from django.urls import path, include
from .views import *

app_name = "places"
urlpatterns = [
    path('<str:place>', render_place, name="place"),
    path('', awal_place, name="awal"),
]