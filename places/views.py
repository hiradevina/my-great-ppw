from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Places, Record
from django.contrib.auth.decorators import login_required
# Create your views here.
response = {}
def awal_place(request):
    places = Places.objects.all()
    response['places'] = places
    return render(request, "home.html", response)


@login_required
def render_place(request, place):
    program = get_object_or_404(Places, pk=place)
    nama = request.user.first_name + request.user.last_name
    print(request.user.username)
    obj = Record(nama=request.user.first_name + request.user.last_name, place=place)
    obj.save()
    response['program'] = program
    return render(request, "places.html", response)